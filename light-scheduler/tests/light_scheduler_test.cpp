#include "CppUTest/TestHarness.h"

extern "C" {
#include "data_type_defs.h"
#include "light_controller_spy.h"
#include "light_scheduler.h"
#include "fake_time_service.h"
}

void set_time_to(const Day day, const i32 minute_of_day) {
    FakeTimeService_set_day(day);
    FakeTimeService_set_minute(minute_of_day);
}

void check_light_state(const i32 id, const i32 level) {
    LONGS_EQUAL(id, LightControllerSpy_get_last_id());
    LONGS_EQUAL(level, LightControllerSpy_get_last_state());
}

TEST_GROUP(LightScheduler) {
	void setup() {
		LightController_create();
		LightScheduler_create();
	}

	void teardown() {
		LightController_destroy();
		LightScheduler_destroy();
	}
};

TEST(LightScheduler, NoChangeToLightsDuringInitialization) {
    LONGS_EQUAL(LIGHT_ID_UNKNOWN, LightControllerSpy_get_last_id());
    LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightControllerSpy_get_last_state());
}

TEST(LightScheduler, NoScheduleNothingHappens) {
    FakeTimeService_set_day(MONDAY);
    FakeTimeService_set_minute(100);

    LightScheduler_wakeup();

    LONGS_EQUAL(LIGHT_ID_UNKNOWN, LightControllerSpy_get_last_id());
    LONGS_EQUAL(LIGHT_ID_UNKNOWN, LightControllerSpy_get_last_state());
}

TEST(LightScheduler, ScheduleOnEverydayNotTimeYet) {
    LightScheduler_schedule_turn_on(3, EVERYDAY, 1200);
    FakeTimeService_set_day(MONDAY);
    FakeTimeService_set_minute(1199);

    LightScheduler_wakeup();

    LONGS_EQUAL(LIGHT_ID_UNKNOWN, LightControllerSpy_get_last_id());
    LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightControllerSpy_get_last_state());
}

TEST(LightScheduler, ScheduleOnEverydayItsTime) {
    LightScheduler_schedule_turn_on(3, EVERYDAY, 1200);
    FakeTimeService_set_day(MONDAY);
    FakeTimeService_set_minute(1200);

    LightScheduler_wakeup();
    LONGS_EQUAL(3, LightControllerSpy_get_last_id());
    LONGS_EQUAL(LIGHT_ON, LightControllerSpy_get_last_state());
}

TEST(LightScheduler, ScheduleOffEverydayItsTime) {
    LightScheduler_schedule_turn_off(3, EVERYDAY, 1200);
    FakeTimeService_set_day(MONDAY);
    FakeTimeService_set_minute(1200);

    LightScheduler_wakeup();
    LONGS_EQUAL(3, LightControllerSpy_get_last_id());
    LONGS_EQUAL(LIGHT_OFF, LightControllerSpy_get_last_state());
}

TEST(LightScheduler, ScheduleWeekEndItsMonday) {
    LightScheduler_schedule_turn_on(3, WEEKEND, 1200);
    set_time_to(MONDAY, 1200);
    LightScheduler_wakeup();
    check_light_state(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleTuesdayButItsMonday) {
    LightScheduler_schedule_turn_on(3, TUESDAY, 1200);
    set_time_to(MONDAY, 1200);
    LightScheduler_wakeup();
    check_light_state(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleTuesdayButItsTuesday) {
    LightScheduler_schedule_turn_on(3, TUESDAY, 1200);
    set_time_to(TUESDAY, 1200);
    LightScheduler_wakeup();
    check_light_state(3, LIGHT_ON);
}

TEST(LightScheduler, ScheduleWeekEndItsFriday) {
    LightScheduler_schedule_turn_on(3, WEEKEND, 1200);
    set_time_to(FRIDAY, 1200);
    LightScheduler_wakeup();
    check_light_state(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleWeekEndItsSaturday) {
    LightScheduler_schedule_turn_on(3, WEEKEND, 1200);
    set_time_to(SATURDAY, 1200);
    LightScheduler_wakeup();
    check_light_state(3, LIGHT_ON);
}

TEST(LightScheduler, ScheduleWeekEndItsSunday) {
    LightScheduler_schedule_turn_on(3, WEEKEND, 1200);
    set_time_to(SUNDAY, 1200);
    LightScheduler_wakeup();
    check_light_state(3, LIGHT_ON);
}
