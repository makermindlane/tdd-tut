#include "light_scheduler.h"

#include <stdbool.h>

#include "light_controller.h"
#include "time_service.h"

enum { UNUSED = -1 };

typedef enum Event { TURN_OFF, TURN_ON } Event;

typedef struct ScheduledLightEvent {
    i32 id;
    Day day;
    i32 minute_of_day;
    Event event;
} ScheduledLightEvent;

static ScheduledLightEvent scheduled_event;

//-------------------------------------------------------------------------------------------------
// Private Function Prototypes
//-------------------------------------------------------------------------------------------------

static void schedule_event(const i32 id, const Day day, const i32 minute_of_day, const i32 event);
static void process_event_due_now(const Time* const time,
                                  const ScheduledLightEvent* const light_event);
static void operate_light(const ScheduledLightEvent* const light_event);

//-------------------------------------------------------------------------------------------------
// Public Functions Implementation
//-------------------------------------------------------------------------------------------------

void LightScheduler_create() { scheduled_event.id = UNUSED; }

void LightScheduler_destroy() {}

void LightScheduler_wakeup() {
    Time time;

    TimeService_get_time(&time);
    process_event_due_now(&time, &scheduled_event);
}

void LightScheduler_schedule_turn_on(i32 id, Day day, i32 minute_of_day) {
    schedule_event(id, day, minute_of_day, TURN_ON);
}

void LightScheduler_schedule_turn_off(i32 id, Day day, i32 minute_of_day) {
    schedule_event(id, day, minute_of_day, TURN_OFF);
}

//-------------------------------------------------------------------------------------------------
// Private Functions Implementation
//-------------------------------------------------------------------------------------------------

static void schedule_event(const i32 id, const Day day, const i32 minute_of_day, const i32 event) {
    scheduled_event.id = id;
    scheduled_event.day = day;
    scheduled_event.minute_of_day = minute_of_day;
    scheduled_event.event = event;
}

static bool does_light_response_today(const Time* const time, Day scheduled_day) {
    Day today = time->day;
    if (scheduled_day == EVERYDAY) return true;
    if (scheduled_day == today) return true;
    if (scheduled_day == WEEKEND) {
        if (today == SATURDAY || today == SUNDAY) return true;
    }
    if (scheduled_day == WEEKDAY) {
        if (today > SUNDAY && today < SATURDAY) return true;
    }
    return false;
}

static void process_event_due_now(const Time* const time,
                                  const ScheduledLightEvent* const light_event) {
    Day schld_day = light_event->day;
    if (light_event->id == UNUSED) return;
    if (light_event->minute_of_day != time->minute_of_day) return;
    if (does_light_response_today(time, schld_day)) {
        operate_light(light_event);
    }
    // if (schld_day != EVERYDAY && schld_day != time->day &&
    //     (schld_day == SATURDAY || schld_day == WEEKEND))
    //     return;
}

static void operate_light(const ScheduledLightEvent* const light_event) {
    if (light_event->event == TURN_ON) {
        LightController_on(light_event->id);
    } else if (light_event->event == TURN_OFF) {
        LightController_off(light_event->id);
    }
}
